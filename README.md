  <b>Zadatak:</b>
  Implementirati Web aplikaciju za rad sa Proizvodima (Id, Naziv, Cena, Godina proizvodnje). <br/>
  Implementirati REST API koji će podržavati registraciju i prijavu korisnika, kao i CRUD operacije sa proizvodima.<br/>
  Uz pomoć jQuery, Bootstrap i Partial Views implementirati SPA klijenta. Ukoliko korisnik nije prijavljen, na toj strani
  može da vidi formu za registrovanje i prijavu, kao i tabelarni prikaz podataka.<br/>
  Nakon što se korisnik prijavi, umesto forme za registrovanje i prijavu može da vidi dugme Odjava koje će ga odjaviti sa sistema.<br/>
  Ispod toga mu je dostupna tabela sa prikazom proizvoda, kao i akcije dodavanja, izmene i brisanja proizvoda.<br/>