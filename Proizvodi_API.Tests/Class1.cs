﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Proizvodi_API.Controllers;
using Proizvodi_API.Interfaces;
using Proizvodi_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Proizvodi_API.Tests
{
    [TestClass]
    public class Class1
    {
        [TestMethod]
        public void GetReturnsCityWithSameId()
        {
            //Arrange
            var mockRepository = new Mock<IProizvodRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Models.Proizvod { Id = 42 });

            var controller = new ProizvodiController(mockRepository.Object);

            //Act 
            IHttpActionResult actionResult = controller.GetProizvod(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Proizvod>;

            //Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }
        [TestMethod]
        public void GetReturnsNotFound()
        {
            //Arrange
            var mockRepository = new Mock<IProizvodRepository>();
            var controller = new ProizvodiController(mockRepository.Object);

            //Act 
            IHttpActionResult actionResult = controller.GetProizvod(20);

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void DeleteReturnsOk()
        {
            //Arrange
            var mockRepository = new Mock<IProizvodRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Proizvod { Id = 10 });
            var controller = new ProizvodiController(mockRepository.Object);

            //Act 
            IHttpActionResult actionResult = controller.DeleteProizvod(10);

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }
        [TestMethod]
        public void PutReturnsBadRequest()
        {
            //Arrange
            var mockRepository = new Mock<IProizvodRepository>();
            var controller = new ProizvodiController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.PutProizvod(10, new Proizvod { Id = 11, Naziv = "asdasd", Cena = 111.3m, KategorijaId = 1  });

            //Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }
        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            //Arrange
            var mockRepository = new Mock<IProizvodRepository>();
            var controller = new ProizvodiController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.PostProizvod(new Proizvod { Id = 11, Naziv = "adasd", Cena = 111, KategorijaId = 1});
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Proizvod>;

            //Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(11, createdResult.RouteValues["id"]);
        }
        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            //Arrange
            List<Proizvod> pro = new List<Proizvod>();
            pro.Add(new Proizvod { Id = 1, Cena = 111, Naziv = "sdasd", KategorijaId = 1});
            pro.Add(new Proizvod { Id = 2, Cena = 222, Naziv = "aaa", KategorijaId = 1 });

            var mockRepository = new Mock<IProizvodRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(pro.AsEnumerable());
            var controller = new ProizvodiController(mockRepository.Object);

            //Act
            IEnumerable<Proizvod> result = controller.GetProizvodi();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(pro.Count, result.ToList().Count);
            Assert.AreEqual(pro.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(pro.ElementAt(1), result.ElementAt(1));

        }
    }
}

