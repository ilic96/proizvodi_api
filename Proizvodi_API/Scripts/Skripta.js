﻿$(window).on(function () {
    $("#proizvodi").click();
});

$(document).ready(function () {
    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create"; // dodavanje proizvoda
    var editingId; // id za PUT

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");
    $("#bReg").click(function () {
        $("#registracijaP").css("display", "block");
        $("#bReg").css("display", "none");
    });
    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };


        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            clearForm();
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        }).fail(function (data) {
            alert(data);
        });
    });

    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");
            $("#proizvodi").trigger("click");
         
        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};

        $("#info").empty();
        $("#data").empty();
        $("#proizvodi").trigger("click");
        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#formDiv").css("display", "none");
        $("#priEmail").val('');
        $("#priLoz").val('');
    
    })

    // pripremanje dogadjaja za brisanje
    $("body").on("click", "#btnDelete", deleteProizvod);

    // priprema dogadjaja za izmenu
    $("body").on("click", "#btnEdit", editProizvod);

    // ucitavanje proizvoda
    $("#proizvodi").click(function () {

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/proizvodi",
            "headers": headers

        }).done(function (data, status) {
            clearForm();
            var $container = $("#data");
            $container.empty();

            if (status === "success") {
                console.log(data);
                // ispis Proizvoda
                if (token) {
                    var div = $("<div></div>");
                    var h1 = $("<h1>Proizvodi</h1>");
                    div.append(h1);

                    // ispis tabele
                    var table = $("<table class='table table-hover table-bordered'></table>");
                    var header = $("<tr><th>ID</th><th>Naziv</th><th>Cena</th><th>Kategorija</th><th>Delete</th><th>Edit</th></tr>");
                   
                    table.append(header);

                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        var row = "<tr>";
                        // prikaz podataka
                        var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Naziv + "</td><td>" + data[i].Cena + "</td><td>" + data[i].Kategorija.Naziv + "</td>";
                        // prikaz dugmadi za izmenu i brisanje
                        var stringId = data[i].Id.toString();
                        var displayDelete = "<td><button class='btn-danger' id=btnDelete name=" + stringId + ">Delete</button></td>";
                        var displayEdit = "<td><button class='btn-primary' id=btnEdit name=" + stringId + ">Edit</button></td>";
                        row += displayData + displayDelete + displayEdit + "</tr>";
                        table.append(row);
                        newId = data[i].Id;

                        console.log(data);
                    }

                    div.append(table);
                    
                    // prikaz forme
                    $("#formDiv").css("display", "block");

                    // ispis novog sadrzaja
                    $container.append(div);

                } else {
                    div = $("<div></div>");
                    h1 = $("<h1>Proizvodi</h1>");
                    div.append(h1);

                    // ispis tabele
                    table = $("<table class='table table-hover table-bordered'></table>");
                    header = $("<tr><th>ID</th><th>Naziv</th><th>Cena</th><th>Kategorija</th></tr>");
                    table.append(header);

                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        row = "<tr>";
                        // prikaz podataka
                        displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Naziv + "</td><td>" + data[i].Cena + "</td><td>" + data[i].Kategorija.Naziv + "</td>";
                        stringId = data[i].Id.toString();

                        row += displayData + "</tr>";
                        table.append(row);
                        newId = data[i].Id;

                        console.log(data);
                    }

                    div.append(table);

                    // ispis novog sadrzaja
                    $container.append(div);
                }

            }
            else {
                div = $("<div></div>");
                h1 = $("<h1>Greška prilikom preuzimanja proizvoda!</h1>");
                div.append(h1);
                $container.append(div);
            }

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });

        
    });

    // Brisanje Proizvoda
    function deleteProizvod() {

        // izvlacimo {id}
        var deleteId = this.name;
        // saljemo zahtev 
        $.ajax({
            "type": "DELETE",
            "url": "http://" + host + "/api/proizvodi/" + deleteId.toString(),
            "headers": headers
        }).done(function (data, status) {
            refreshTable();
        }).fail(function (data, status) {
            alert("Desila se greska!");
        });

    };

    // Izmena Proizvoda
    function editProizvod() {
        // izvlacimo id
        var editId = this.name;
        // saljemo zahtev da dobavimo taj proizvod
        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/proizvodi/" + editId.toString(),
            "headers": headers
        }).done(function (data, status) {
            $("#proizvodNaziv").val(data.Naziv);
            $("#proizvodCena").val(data.Cena);
            $("#proizvodKategorija").val(data.KategorijaId);
            editingId = data.Id;
            formAction = "Update";
        }).fail(function (data, status) {
            formAction = "Create";
            alert("Desila se greska!");
        });

    };

    // add i edit proizvoda
    $("#proizvodForm").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        var proizvodNaziv = $("#proizvodNaziv").val();
        var proizvodCena = $("#proizvodCena").val();
        var proizvodKategorija = $("#proizvodKategorija").val();
        var httpAction;
        var sendData;
        var url;

        // u zavisnosti od akcije pripremamo objekat
        if (formAction === "Create") {
            httpAction = "POST";
            url = "http://" + host + "/api/proizvodi/";
            console.log("URL:" + url);
            sendData = {
                "Naziv": proizvodNaziv,
                "Cena": proizvodCena,
                "KategorijaId": proizvodKategorija,

            };
        }
        else {
            httpAction = "PUT";
            url = "http://" + host + "/api/proizvodi/" + editingId.toString();
            sendData = {
                "Id": editingId,
                "Naziv": proizvodNaziv,
                "Cena": proizvodCena,
                "KategorijaId": proizvodKategorija,
            };
        }

        console.log("Objekat za slanje");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: sendData
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            })

    });

    // osvezavanje prikaz tabele
    function refreshTable() {
        // cistimo formu
        $("#proizvodNaziv").val('');
        $("#proizvodCena").val('');
        $("#kategorijaId").val('');

        // osvezavamo
        $("#proizvodi").trigger("click");
    };

    // osvezavanje forme
    function clearForm() {
        // clear forme
        $("#proizvodNaziv").val('');
        $("#proizvodCena").val('');
        $("#proizvodKategorija").val('');


        // clear registracija
        $("#regEmail").val('').empty();
        $("#regLoz").val('').empty();
        $("#regLoz2").val('').empty();
    };

});