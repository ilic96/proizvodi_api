﻿using Proizvodi_API.Interfaces;
using Proizvodi_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Proizvodi_API.Controllers
{
    public class ProizvodiController : ApiController
    {
        private IProizvodRepository _repository { get; set; }

        public ProizvodiController(IProizvodRepository repository)
        {
            _repository = repository;
        }
        //GET api/proizvodi
        [ResponseType(typeof(Proizvod))]
        public IEnumerable<Proizvod> GetProizvodi()
        {
            return _repository.GetAll();
        }
        //GET api/proizvodi/1
        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult GetProizvod(int id)
        {
            var proizvod = _repository.GetById(id);
            if(proizvod == null)
            {
                return NotFound();
            }
            return Ok(proizvod);
        }
        //POST api/proizvodi/1
        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult PostProizvod(Proizvod proizvod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(proizvod);
            return CreatedAtRoute("DefaultApi", new { id = proizvod.Id }, proizvod);
        }
        //PUT api/proizvodi/1
        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult PutProizvod(int id, Proizvod proizvod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id != proizvod.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(proizvod);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(proizvod);
        }
        //DELETE api/proizvodi/1
        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult DeleteProizvod(int id)
        {
            var proizvod = _repository.GetById(id);
            if(proizvod == null)
            {
                return NotFound();
            }
            _repository.Delete(proizvod);
            return Ok();
        }
        //GET api/proizvodi?x=500
        [ResponseType(typeof(Proizvod))]
        public IEnumerable<Proizvod> GetFilter(int x)
        {
            return _repository.Filter(x);
        }
    }
}
