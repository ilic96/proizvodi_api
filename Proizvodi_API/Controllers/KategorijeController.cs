﻿using Proizvodi_API.Interfaces;
using Proizvodi_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Proizvodi_API.Controllers
{
    public class KategorijeController : ApiController
    {
        private IKategorijaRepository _repository { get; set; }

        public KategorijeController(IKategorijaRepository repository)
        {
            _repository = repository;
        }
        //GET api/kategorije
        [ResponseType(typeof(Kategorija))]
        public IEnumerable<Kategorija> GetKategorije()
        {
            return _repository.GetAll();
        }
        //GET api/kategorije/1
        [ResponseType(typeof(Kategorija))]
        public IHttpActionResult GetKategorija(int id)
        {
            var kategorija = _repository.GetById(id);
            if(kategorija == null)
            {
                return NotFound();
            }
            return Ok(kategorija);
        }
        //POST api/kategorije/1
        [ResponseType(typeof(Kategorija))]
        public IHttpActionResult PostKategorija(Kategorija kategorija)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(kategorija);
            return CreatedAtRoute("DefaultApi", new { id = kategorija.Id }, kategorija);
        }
        //PUT api/kategorije/1
        [ResponseType(typeof(Kategorija))]
        public IHttpActionResult PutKategorija(int id, Kategorija kategorija)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id != kategorija.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(kategorija);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(kategorija);
        }
        //DELETE api/kategorije/1
        [ResponseType(typeof(Kategorija))]
        public IHttpActionResult DeleteKategorija(int id)
        {
            var kategorija = _repository.GetById(id);
            if(kategorija == null)
            {
                return NotFound();
            }
            _repository.Delete(kategorija);
            return Ok();
        }
    }
}
