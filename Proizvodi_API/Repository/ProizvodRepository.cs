﻿using Proizvodi_API.Interfaces;
using Proizvodi_API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Proizvodi_API.Repository
{
    public class ProizvodRepository : IDisposable, IProizvodRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Proizvod proizvod)
        {
            db.Proizvodi.Add(proizvod);
            db.SaveChanges();
        }

        public void Delete(Proizvod proizvod)
        {
            db.Proizvodi.Remove(proizvod);
            db.SaveChanges();
        }

        public IEnumerable<Proizvod> GetAll()
        {
            return db.Proizvodi.Include(k => k.Kategorija);
        }

        public Proizvod GetById(int id)
        {
            return db.Proizvodi.FirstOrDefault(p => p.Id == id);
        }

        public void Update(Proizvod proizvod)
        {
            db.Entry(proizvod).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Proizvod> Filter(int x)
        {
            IEnumerable<Proizvod> query = from p in db.Proizvodi.Include(k => k.Kategorija).OrderBy(c => c.Cena)
                                          where p.Cena < x
                                          select p;
            return query;
        }
    }
}