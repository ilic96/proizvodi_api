﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Proizvodi_API.Models
{
    public class Proizvod
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Naziv { get; set; }
        [Required]
        [Range(0, 1000.0)]
        public decimal Cena { get; set; }
        [ForeignKey("Kategorija")]
        public int KategorijaId { get; set; }
        public Kategorija Kategorija { get; set; }
    }
}