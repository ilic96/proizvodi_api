﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proizvodi_API.Models
{
    public class Kategorija
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Naziv { get; set; }
    }
}