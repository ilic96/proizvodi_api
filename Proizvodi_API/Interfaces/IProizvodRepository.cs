﻿using Proizvodi_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proizvodi_API.Interfaces
{
    public interface IProizvodRepository
    {
        IEnumerable<Proizvod> GetAll();
        IEnumerable<Proizvod> Filter(int x);
        Proizvod GetById(int id);
        void Add(Proizvod proizvod);
        void Update(Proizvod proizvod);
        void Delete(Proizvod proizvod);
    }
}
