﻿using Proizvodi_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proizvodi_API.Interfaces
{
    public interface IKategorijaRepository
    {
        IEnumerable<Kategorija> GetAll();
        Kategorija GetById(int id);
        void Add(Kategorija kategorija);
        void Update(Kategorija kategorija);
        void Delete(Kategorija kategorija);
    }
}
