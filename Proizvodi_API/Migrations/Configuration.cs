namespace Proizvodi_API.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Proizvodi_API.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Proizvodi_API.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Kategorije.AddOrUpdate
                (
                    new Models.Kategorija() { Id = 1, Naziv = "Odeca"},
                    new Models.Kategorija() { Id = 2, Naziv = "Obuca"}
                );
            context.SaveChanges();
            context.Proizvodi.AddOrUpdate
                (
                    new Models.Proizvod() { Id = 1, Naziv = "Majica", Cena = 899.9m, KategorijaId = 1},
                    new Models.Proizvod() { Id = 2, Naziv = "Duks", Cena = 920.4m, KategorijaId = 1 },
                    new Models.Proizvod() { Id = 3, Naziv = "Patike", Cena = 789.33m, KategorijaId = 2},
                    new Models.Proizvod() { Id = 4, Naziv = "Cipele", Cena = 900.55m, KategorijaId = 2}
                );
            context.SaveChanges();
        }
    }
}
